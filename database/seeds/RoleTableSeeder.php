<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{

    protected $roles = [
        '1' => 'admin',
        '2' =>'moderator',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks=0');
        DB::table('roles')->truncate();

        foreach ($this->roles as $key => $value) {
            $newRole = new Role();
            $newRole->id = $key;
            $newRole->name = $value;
            $newRole->save();
        }
    }
}
