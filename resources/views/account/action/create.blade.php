@extends('layouts.account.main')

@section('content')
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">Create action</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('actions.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="input-id">File input</label>
                        <input type="file" name="image" id="input-id">
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" class="form-control" required>
                    </div>
                    <div id="map">

                    </div>
                    <input id="map-input-lat" type="text" name="position_lat" class="hidden">
                    <input id="map-input-lng" type="text" name="position_lng" class="hidden">

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

                <form action="{{ route('locate') }}" method="POST">
                    {{ csrf_field() }}

                    <input type="text" name="locate">

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="panel-footer"><a href="{{ route('profile.index') }}">Назад</a></div>
        </div>
    </div>
@endsection

@push('css-libraries')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@endpush

@push('js-libraries')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/locales/LANG.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush

@section('scripts')
    <script>
        $( document ).ready(function() {
            $("#input-id").fileinput({
                showUpload: false,
                showClose: false,
            });
        });

        function initMap() {
            var position_lat = 48.464738;
            var position_lng = 35.046253;
            var position = {lat: position_lat, lng: position_lng};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: position
            });

            var marker = new google.maps.Marker({
                position: position,
                map: map,
            });

            google.maps.event.addListener(map, 'click', function (event) {
                position_lat = event.latLng.lat();
                position_lng = event.latLng.lng();
                marker.setPosition(event.latLng);
                $('#map-input-lat').val(position_lat);
                $('#map-input-lng').val(position_lng);
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2mkaso49C_C9CKget2mNMbWjqJBkXe9Y&callback=initMap"></script>
@endsection