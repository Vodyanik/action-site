@extends('layouts.account.main')

@section('content')
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <a href="{{ route('actions.create') }}" class="btn btn-success">Create new action</a>
            </div>
            <div class="panel-body">
                @foreach($actions as $action)
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ Storage::url($action->images()->first()->path) }}" width="100%">
                        </div>
                        <div class="col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item">{{ $action->title }}</li>
                                <li class="list-group-item">{{ $action->description }}</li>
                            </ul>
                        </div>
                    </div>
                    <hr>
                @endforeach

                @if($actions->isEmpty())
                    <h3 class="text-center">there is not nothing</h3>
                @endif
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
@endsection