@extends('layouts.account.main')

@section('content')
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">User profile</div>
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">Id: {{ $user->id }}</li>
                    <li class="list-group-item">Name: {{ $user->name }}</li>
                    <li class="list-group-item">Email: {{ $user->email }}</li>
                </ul>
            </div>
            <div class="panel-footer"><a href="{{ route('profile.edit', ['id' => $user->id]) }}">Редактировать</a></div>
        </div>
    </div>
@endsection