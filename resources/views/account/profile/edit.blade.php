@extends('layouts.account.main')

@section('content')
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">Edit profile</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('profile.update', ['id' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

                    <hr>


                <form action="{{ route('profile.update_password', ['id' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label>New password</label>
                        <input type="password" name="password" class="form-control" placeholder="********">
                    </div>
                    <div class="form-group">
                        <label>Confirm password</label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="********">
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="panel-footer"><a href="{{ route('profile.index') }}">Назад</a></div>
        </div>
    </div>
@endsection