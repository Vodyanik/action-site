<div class="col-md-4">
    <div class="list-group">
        <a href="{{ route('profile.index') }}" class="list-group-item {{ Request::is('profile', 'profile/*') ? "active" : '' }}">Profile</a>
        <a href="{{ route('actions.index') }}" class="list-group-item {{ Request::is('actions', 'actions/*') ? "active" : '' }}">Actions</a>
        <a href="#" class="list-group-item {{ Request::is('') ? "active" : '' }}">Bookmarks</a>
    </div>
</div>