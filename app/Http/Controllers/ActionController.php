<?php

namespace App\Http\Controllers;

use App\Helpers\Contracts\ImageContract;
use App\Helpers\Contracts\Repositories\ActionRepository;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ActionController extends Controller
{
    protected $actionRepository;

    public function __construct(ActionRepository $actionRepository)
    {
        $this->actionRepository = $actionRepository;
    }

    public function locate(Request $request)
    {
        $client = new Client();
//        $res = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Киев&types=(cities)&language=ru_RU&key=' . env('GOOGLE_API_KEY'));
//        dd(\GuzzleHttp\json_decode($res->getBody()));
        $result = $client->get('https://maps.googleapis.com/maps/api/place/autocomplete/json?', [
            'query' => [
                'input' => 'Киев',
                'components' => 'country:ukr',
                'language' => 'ru_RU',
                'key' => env('GOOGLE_API_KEY'),
            ]
        ]);
        dd(\GuzzleHttp\json_decode($result->getBody()));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actions = $this->actionRepository->all();

        return view('account.action.index', compact('actions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account.action.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageContract $image)
    {
        $this->actionRepository->store($request, $image);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
