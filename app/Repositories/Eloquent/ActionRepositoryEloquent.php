<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use GuzzleHttp\Client;

use Illuminate\Support\Facades\Storage;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Helpers\Contracts\Repositories\ActionRepository;
use App\Helpers\Contracts\ImageContract;

use App\Models\Action;
use App\Models\Image;

/**
 * Class ActionRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class ActionRepositoryEloquent extends BaseRepository implements ActionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Action::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function store(Request $request, ImageContract $image)
    {
        $client = new Client();
//        $res = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Киев&types=(cities)&language=ru_RU&key=' . env('GOOGLE_API_KEY'));
//        dd(\GuzzleHttp\json_decode($res->getBody()));
        $result = $client->post('https://maps.googleapis.com/maps/api/geocode/json?', [
            'query' => [
                'latlng' => $request->position_lat . "," . $request->position_lng,
                'result_type' => 'street_number|route|locality|country',
//                'components' => 'country',
//                'language' => 'ru',
                'key' => env('GOOGLE_API_KEY'),
            ]
        ]);

        dd(\GuzzleHttp\json_decode($result->getBody()));

        $action = new Action();
        $action->fill($request->all());
        $action->user_id = Auth::user()->id;
        $action->country_id = 1;
        $action->city_id = 1;

        $actionImage = new Image();
        $actionImage->path = $image->save($request->file('image'), Auth::user()->id);

        DB::transaction(function () use ($action, $actionImage) {
            $action->save();
            $action->images()->save($actionImage);
        });
    }
}
