<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function create(User $user)
    {
        if ($user->id == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function before(User $user, $ability)
    {
//        dd("eqeq");
//        if ($user->id == 1) {
//            return 'qwe';
//        }
    }
}
