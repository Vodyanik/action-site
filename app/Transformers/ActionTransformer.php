<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Action;

/**
 * Class ActionTransformer
 * @package namespace App\Transformers;
 */
class ActionTransformer extends TransformerAbstract
{

    /**
     * Transform the \Action entity
     * @param \Action $model
     *
     * @return array
     */
    public function transform(Action $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
