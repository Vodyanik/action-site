<?php

namespace App\Helpers\Realizations;

use App\Helpers\Contracts\ImageContract;
use File;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * Class ImageIntervention
 * @package App\Helpers\Realizations
 */
class ImageIntervention implements ImageContract {

    /**
     * @param $image
     * @param $path
     * @return mixed
     */
    public function save($image, $path)
    {
        if (!File::exists(storage_path("app/public/" . $path))) {
            File::makeDirectory(storage_path("app/public/" . $path), 0777, true, true);
        }
        $name = $image->getClientOriginalName();
        $path = $path . '/' . $name;
        Image::make($image)->save(storage_path("app/public/" . $path));

        return $path;
    }

}