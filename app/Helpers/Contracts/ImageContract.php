<?php

namespace App\Helpers\Contracts;

/**
 * Interface ImageContract
 * @package App\Helpers\Contracts
 */
interface ImageContract {

    /**
     * @param $image
     * @param $path
     * @return string
     */
    public function save($image, $path);

}