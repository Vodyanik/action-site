<?php

namespace App\Helpers\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CountryRepository
 * @package namespace App\Helpers\Contracts\Repositories;
 */
interface CountryRepository extends RepositoryInterface
{
    //
}
