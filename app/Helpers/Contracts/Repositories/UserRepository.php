<?php

namespace App\Helpers\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository
 * @package namespace App\Helpers\Contracts\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
