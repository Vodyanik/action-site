<?php

namespace App\Helpers\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ActionRepository
 * @package namespace App\Helpers\Contracts\Repositories;
 */
interface ActionRepository extends RepositoryInterface
{
    //
}
