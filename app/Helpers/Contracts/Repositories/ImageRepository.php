<?php

namespace App\Helpers\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ImageRepository
 * @package namespace App\Helpers\Contracts\Repositories;
 */
interface ImageRepository extends RepositoryInterface
{
    //
}
