<?php

namespace App\Helpers\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CityRepository
 * @package namespace App\Helpers\Contracts\Repositories;
 */
interface CityRepository extends RepositoryInterface
{
    //
}
