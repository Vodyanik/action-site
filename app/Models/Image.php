<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Image extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'path',
        'imagetable_id',
        'imagetable_type',
    ];

    public function imagetable()
    {
        return $this->morphTo();
    }

}
