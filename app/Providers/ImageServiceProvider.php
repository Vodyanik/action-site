<?php

namespace App\Providers;

use App\Helpers\Contracts\ImageContract;
use App\Helpers\Realizations\ImageIntervention;
use Illuminate\Support\ServiceProvider;

class ImageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImageContract::class, ImageIntervention::class);
    }
}
