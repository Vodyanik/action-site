<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Helpers\Contracts\Repositories\CountryRepository::class, \App\Repositories\Eloquent\CountryRepositoryEloquent::class);
        $this->app->bind(\App\Helpers\Contracts\Repositories\CityRepository::class, \App\Repositories\Eloquent\CityRepositoryEloquent::class);
        $this->app->bind(\App\Helpers\Contracts\Repositories\RoleRepository::class, \App\Repositories\Eloquent\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Helpers\Contracts\Repositories\ActionRepository::class, \App\Repositories\Eloquent\ActionRepositoryEloquent::class);
        $this->app->bind(\App\Helpers\Contracts\Repositories\ImageRepository::class, \App\Repositories\Eloquent\ImageRepositoryEloquent::class);
        //:end-bindings:
    }
}
