<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::group(['middleware' => 'can:userid'], function () {
//    Route::get('/home', 'HomeController@index')->name('home');
//});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::post('profile/{id}/update-password', ['as' => 'profile.update_password', 'uses' => 'UserController@updatePassword']);
    Route::resource('profile', 'UserController');

    Route::post('locate', ['as' => 'locate', 'uses' => 'ActionController@locate']);
    Route::resource('actions', 'ActionController');
});
